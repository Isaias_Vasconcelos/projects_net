﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Generator.Model;

namespace Generator
{
    internal class MainClass
    {
        static string content;
        
        static HttpClient client1 = new HttpClient { BaseAddress = new Uri("https://viacep.com.br/") };
        public static async Task Main()
        {
            try
            {
                Console.WriteLine("Digite seu cep : ");
                var cep = Console.ReadLine();

                Console.Clear();
                
                Console.WriteLine("...");
                HttpResponseMessage message = client1.GetAsync($"ws/{cep}/json/").Result;
              
                if (message != null)
                {
                 content = await message.Content.ReadAsStringAsync();
                }


                var contentDes = JsonConvert.DeserializeObject<CepModel>(content);
                Console.Clear();

                Console.WriteLine($"Logradouro : {contentDes.Logradouro}");
                Console.WriteLine();
                Console.WriteLine($"Complemento : {contentDes.Complemento}");
                Console.WriteLine();
                Console.WriteLine($"Bairro : {contentDes.Bairro}");
                Console.WriteLine();
                Console.WriteLine($"Localidade : {contentDes.Localidade}");
                Console.WriteLine();
                Console.WriteLine($"UF : {contentDes.UF} ");
                Console.WriteLine();
                Console.WriteLine($"DDD : {contentDes.DDD}");
                Console.WriteLine();
                Console.WriteLine($"Ibge : {contentDes.Ibge}");
                Console.WriteLine();
                Console.WriteLine($"Gia : {contentDes.Gia}");
                Console.WriteLine();
                Console.WriteLine($"Siafi : {contentDes.Siafi}");

            }
            catch (Exception)
            {

                Console.WriteLine("ERROR 404");
            }
          

        }
    }
}
